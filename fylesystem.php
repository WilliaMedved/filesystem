<?php
//Пусть в корне вашего сайта лежит файл test.txt. Считайте данные из этого файла и выведите их на экран.
//fclose($filetxt1);Нужно закрывать
$filetxt1 ="test.txt";
echo file_get_contents($filetxt1);


//Пусть в корне вашего сайта лежит файл test.txt.
//Запишите в него текст '12345'.
$string = 'hi is STRING';
fwrite($filetxt1, $string);


//Создайте файл test.txt и запишите в него текст '12345'.
//Пусть изначально файла с таким именем не существует.
unlink($filetxt1, "text.txt");
$filetxt1 = '';
$str = '12345';
file_put_contents($filetxt1, $str);


//Пусть в корне вашего сайта лежит файл test.txt, в котором записан текст '12345'.
//Откройте этот файл, запишите в конец текста восклицательный знак и сохраните новый текст обратно в файл.
$text = file_get_contents('text.txt');
file_put_contents('text.txt', $text . '!');


//Пусть в корне вашего сайта лежит файл count.txt.
//Изначально в нем записано число 0.
//Сделайте так, чтобы при обновлении страницы наш скрипт каждый раз увеличивал это число на 1.
//То есть у нас получится счетчик обновления страницы в виде файла.    
$i = file_get_contents('count.txt');
file_put_contents('count.txt', $i + 1);


//Пусть в корне вашего сайта лежат файлы     1.txt, 2.txt и 3.txt.
//Вручную сделайте массив с именами этих файлов.
//Переберите его циклом, прочитайте содержимое каждого из файлов, слейте его в одну строку и запишите в новый файл new.txt.
//Изначально этого файла не должно быть.    
$arr = ['1.txt', '2.txt', '3.txt'];
$text1 = '';
foreach($arr as $v) {
    $text1 .= file_get_contents("$v");
}
file_put_contents('new.txt', $text1);


//Пусть в корне вашего сайта лежит файл old.txt.
//Переименуйте его на new.txt.    
rename('old.txt', 'new.txt');


//Пусть в корне вашего сайта лежит файл test1.txt.
//Пусть также в корне вашего сайта лежит папка dir.
//Переместите файл в эту папку.
rename('text.txt', 'dir/text.txt');


//Пусть в корне вашего сайта лежит файл test.txt.
//Скопируйте его в файл copy.txt.
copy('text.txt', 'copy.txt');


//Пусть в корне вашего сайта лежит файл test.txt.
//Удалите его.
unlink('text.txt');


//Проверьте, лежит ли в корне вашего сайта файл test.txt.
var_dump(file_exists('text.txt'));


//Пусть в корне вашего сайта лежит файл test.txt.
//Узнайте его размер, выведите на экран.
echo filesize('text.txt');


//Положите в корень вашего сайта какую-нибудь картинку размером более мегабайта.
//Узнайте размер этого файла и переведите его в мегабайты.
echo filesize('1.jpg')/1024/1024;


//Дан файл test.txt.
//Прочитайте его текст, получите массив его строк.    
$arr = explode(PHP_EOL, file_get_contents('text.txt'));
echo '<pre>';
var_dump($arr);


//Дан файл test.txt. В нем на каждой строке написано какое-то число.
//С помощью функции file найдите сумму этих чисел и выведете ее на экран.
$arr = file('text.txt', FILE_IGNORE_NEW_LINES);
echo array_sum($arr);


//Создайте в корне вашего сайта папку с названием dir.
mkdir('dir');


//Создайте в корне вашего сайта папку с названием test.Затем создайте в этой папке 3 файла: 1.txt, 2.txt, 3.txt.
$arr = ['1.txt', '2.txt', '3.txt'];
mkdir('test');
foreach ($arr as $elem) {
    file_put_contents('test/'.$elem, '');
}


//Удалите папку с названием dir.
rmdir('dir');


//Пусть в корне вашего сайта лежит папка old.
//Переименуйте ее на new.
rename('old', 'new');


//Пусть в корне вашего сайта лежит папка dir,
//а в ней какие-то текстовые файлы.Переберите эти файлы циклом и выведите их тексты в браузер.
$files = array_diff(scandir('dir') ['.', '..']);
foreach ($files as $elem) {
    echo file_get_contents('dir/'.$elem).'<br/>';
}


//Пусть дан путь к файлу.Проверьте, файл это или папка.
$files = array_diff(scandir('new'), ['.', '..']);
foreach ($files as $file) {
    if(is_file('new/'.$file)) {
        echo $file.' - это файл';
    }
    else if(is_dir('new/'.$file)) {
        echo $file.' - это папка';
    }
}


//Сделайте форму для загрузки 1 картинки.
//Напишите PHP код для валидации и загрузки картинки в отдельную папку в проекте.
?>
<html>
<head>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
  <input type="file" name="file">
  <input type="submit" value="Отправить">
</form>
<body/>
</html>
<?php
$pict = getimagesize("test.jpg",$inf);
$image = getimagesize($pict);
$limitBytes  = 1024 * 1024 * 5;
$limitWidth  = 1280;
$limitHeight = 768;
if (filesize($pict) > $limitBytes) die('Размер изображения не должен превышать 5 Мбайт.');
if ($image[1] > $limitHeight)          die('Высота изображения не должна превышать 768 точек.');
if ($image[0] > $limitWidth)           die('Ширина изображения не должна превышать 1280 точек.');
$name = md5_file($pict);
$extension = image_type_to_extension($image[2]);
$format = str_replace('jpeg', 'jpg', $extension);
if (!move_uploaded_file($pict, __DIR__ . '/pics/' . $name . $format)) {
    die('При записи изображения на диск произошла ошибка.');
}


//Сделайте форму для загрузки нескольких картинок.
//Напишите PHP код для валидации и загрузки картинок в отдельную папку в проекте.
<html>
<head>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
  <input type="file" name="file[]" multiple>
  <input type="submit" value="Отправить">
</form>
<body/>
</html>
<?php
$pict = getimagesize("test.jpg",$inf);
$image = getimagesize($pict);
$limitBytes  = 1024 * 1024 * 5;
$limitWidth  = 1280;
$limitHeight = 768;
if (filesize($pict) > $limitBytes) die('Размер изображения не должен превышать 5 Мбайт.');
if ($image[1] > $limitHeight)          die('Высота изображения не должна превышать 768 точек.');
if ($image[0] > $limitWidth)           die('Ширина изображения не должна превышать 1280 точек.');
$name = md5_file($pict);
$extension = image_type_to_extension($image[2]);
$format = str_replace('jpeg', 'jpg', $extension);
if (!move_uploaded_file($pict, __DIR__ . '/pics/' . $name . $format)) {
    die('При записи изображения на диск произошла ошибка.');
}